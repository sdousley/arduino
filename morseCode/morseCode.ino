int LED = 13;

int UNIT = 200;

int DOT = UNIT;
int DASH = UNIT * 3;
int LETTERSPACE = UNIT;
int SPACE = UNIT * 7;

void setup() {
  pinMode (LED, OUTPUT);
}

void loop() {
  h();
  e();
  l();
  l();
  o();
  space();
  w();
  o();
  r();
  l();
  d();
  space();
  space();
}

void letterSpace() {
  delay(LETTERSPACE);
}

void a() {
  dot();
  dash();
  letterSpace();
}

void b() {
  dash();
  dot();
  dot();
  dot();
  letterSpace();
}

void c() {
  dash();
  dot();
  dash();
  dot();
  letterSpace();
}

void d() {
  dash();
  dot();
  dot();
  letterSpace();
}

void e() {
  dot();
  letterSpace();
}

void f() {
  dot();
  dot();
  dash();
  dot();
  letterSpace();
}

void g() {}

void h() {
  dot();
  dot();
  dot();
  dot();
  letterSpace();
}

void i() {
  dot();
  dot();
  letterSpace();
}

void j() {
  dot();
  dash();
  dash();
  dash();
  letterSpace();
}

void k() {
  dash();
  dot();
  dash();
  letterSpace();
}

void l() {
  dot();
  dash();
  dot();
  dot();
  letterSpace();
}

void m() {
  dash();
  dash();
  letterSpace();
}

void n() {
  dash();
  dot();
  letterSpace();
}

void o() {
  dash();
  dash();
  dash();
  letterSpace();
}

void p() {
  dot();
  dash();
  dash();
  dot();
  letterSpace();
}

void q() {
  dash();
  dash();
  dot();
  dash();
  letterSpace();
}

void r() {
  dot();
  dash();
  dot();
  letterSpace();
}

void s() {
  dot();
  dot();
  dot();
  letterSpace();
}

void t() {
  dash();
  letterSpace();
}

void u() {
  dot();
  dot();
  dash();
  letterSpace();
}

void v() {
  dot();
  dot();
  dot();
  dash();
  letterSpace();
}

void w() {
  dot();
  dash();
  dash();
  letterSpace();
}

void x() {
  dash();
  dot();
  dot();
  dash();
  letterSpace();
}

void y() {
  dash();
  dot();
  dash();
  dash();
  letterSpace();
}

void z() {
  dash();
  dash();
  dot();
  dot();
  letterSpace();
}

void num0() {
  dash();
  dash();
  dash();
  dash();
  dash();
  letterSpace();
}

void num1() {
  dot();
  dash();
  dash();
  dash();
  dash();
  letterSpace();
}

void num2() {
  dot();
  dot();
  dash();
  dash();
  dash();
  letterSpace();
}

void num3() {
  dot();
  dot();
  dot();
  dash();
  dash();
  letterSpace();
}

void num4() {
  dot();
  dot();
  dot();
  dot();
  dash();
  letterSpace();
}

void num5() {
  dot();
  dot();
  dot();
  dot();
  dot();
  letterSpace();
}

void num6() {
  dash();
  dot();
  dot();
  dot();
  dot();
  letterSpace();
}

void num7() {
  dash();
  dash();
  dot();
  dot();
  dot();
  letterSpace();
}

void num8() {
  dash();
  dash();
  dash();
  dot();
  dot();
  letterSpace();
}

void num9() {
  dash();
  dash();
  dot();
  dot();
  dot();
  letterSpace();
}

void dot() {
  digitalWrite(LED, HIGH);
  delay(DOT);
  digitalWrite(LED, LOW);
  delay(LETTERSPACE);
}

void dash() {
  digitalWrite(LED, HIGH);
  delay(DASH);
  digitalWrite(LED, LOW);
  delay(LETTERSPACE);  
}

void space() {
  delay(SPACE); 
}

